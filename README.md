## Instructions
* Get connected to the database using PSQL command(Postgres-client)
* Let's create a sample database
    * create database demo;
* Switch to the demo database
    * \c demo;
* Create a table in the datbase which we created
```
create table employee(
id SERIAL,
name varchar(50),
job varchar(40),
department varchar(40),
salary int,
hire_date date,
PRIMARY KEY (id));
```
* Exit out of that database
    * \q
* Now we are back on the VM, let's install NodeJs and NPM
    * apt-get install nodejs npm
* Let's clone the code from repository
    * git clone https://gitlab.com/synechron-azure-august-batch/application-insights-node
    * cd application-insights-node
* We need to update the connection string in the employee.js
    * vi routes/employees.js
* Install the node libraries
    * npm install
* You need to allow the traffic on port 4000 of this VM, as this application runs on that port only.
* This code is written for NonSSL based database, so you need to disable the SSL enforcment in Azure
    * Go to DB Page in Azure -> Connection Security -> Just disbale the Enforce SSL in that
* To enable the monitoring with applicaiton insights, we need to update the Instrumentation Key in app.js file
    * vi app.js
* Installation of Application Insights npm package
    * npm install applicationinsights --save
* Let's run the application
    * node app.js
* Now, browse your application in browser at http://vm_ip:4000
* Give some sort of hits to your app, the go Application Insights console, you will see your data over there

